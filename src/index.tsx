import React from "react";
import ReactDOM from "react-dom";
import { Router } from "react-router-dom";
import { Provider } from "react-redux";
import "./index.css";
import "semantic-ui-css/semantic.min.css";
import configureStore from "./store";
import history from "./history";
import routes from "./routes";

ReactDOM.render(
  <Provider store={configureStore()}>
    <Router history={history}>
      {routes}
    </Router>
  </Provider>,
  document.getElementById("root") as HTMLElement
);
