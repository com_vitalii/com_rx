import React from "react";
import { Router, Route, Switch } from "react-router-dom";
import App from "../containers/App/index";
import Signup from "../containers/Signup";
import Login from "../containers/Login";
import NotFoundPage from "../components/NotFoundPage";
import PrivateRoute from "./PrivateRoute";
import history from "../history";
import AboutUs from "../components/AboutUs";
import ContactUs from "../components/ContactUs";

const routes = (
  <Router history={history}>
    <Switch>
      <PrivateRoute exact path="/" component={App} />
      <PrivateRoute path="/ssss" component={App} />
      <PrivateRoute path="/about-us" component={AboutUs} />
      <PrivateRoute path="/contact-us" component={ContactUs} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/sign-up" component={Signup} />
      <Route path="*" component={NotFoundPage} />
    </Switch>
  </Router>
);

export default routes;
