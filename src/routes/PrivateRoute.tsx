import React from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import HeaderHelmet from "../containers/HeaderHelmet";

class PrivateRoute extends React.Component<any, any> {
  state = {
    token: localStorage.getItem("token")
  };

  render() {
    const { component: Component, auth, ...rest } = this.props;
    return (
      <Route
        {...rest}
        render={props =>
          auth.isAuthenticated || this.state.token ? (
            <>
              <HeaderHelmet/>
              <Component {...props} />
            </>
          ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: props.location }
              }}
            />
          )
        }
      />
    );
  }
}
const mapStateToProps = (state: any) => {
  return {
    auth: state.auth
  };
};

export default connect(
  mapStateToProps,
  {}
)(PrivateRoute);
