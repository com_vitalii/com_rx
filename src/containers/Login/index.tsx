import React from "react";
import {
  Button,
  Form,
  Grid,
  Header,
  Message,
  Segment
} from "semantic-ui-react";
import { Field, reduxForm, InjectedFormProps } from "redux-form";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import TextField from "../../components/TextField";
import { loginRequest } from "../../actions/auth";

interface StateProps {
  valueFromState: string;
}

interface Props extends StateProps {
  loading: boolean;
  loginRequest: any;
  loggedIn: boolean;
  history: any;
}
interface ILoginValues {
  email: string;
  password: string;
}
class Login extends React.Component<Props & InjectedFormProps<{}, Props>> {
  state = {
    token: localStorage.getItem("token")
  };

  componentDidMount() {
    if (this.props.loggedIn || this.state.token) {
      this.props.history.push("/");
    }
  }
  componentDidUpdate() {
    if (this.props.loggedIn || this.state.token) {
      this.props.history.push("/");
    }
  }
  
  handleLogin = (values: ILoginValues | any): any => {
    this.props.loginRequest(values);
  };

  render() {
    const { error, handleSubmit, pristine, reset, submitting } = this.props;
    return (
      <div className="login-form">
        <style>{`
      body > div,
      body > div > div,
      body > div > div > div.login-form {
        height: 100%;
      }
    `}</style>
        <Grid
          textAlign="center"
          style={{ height: "100%" }}
          verticalAlign="middle"
        >
          <Grid.Column style={{ maxWidth: 450 }}>
            <Header as="h2" color="grey" textAlign="center">
              Log in
            </Header>
            <Form size="large" onSubmit={handleSubmit(this.handleLogin)}>
              <Segment stacked>
                <Field
                  name="email"
                  component={TextField}
                  fluid
                  icon="user"
                  iconPosition="left"
                  placeholder="E-mail address"
                />
                <Field
                  name="password"
                  component={TextField}
                  fluid
                  icon="lock"
                  iconPosition="left"
                  placeholder="Password"
                  type="password"
                />
                <Button color="green" fluid size="large" type="submit">
                  Login
                </Button>
                <Link to="/ssss">Ssss</Link>
              </Segment>
            </Form>
            <Message>
              New to us? <Link to="/sign-up">Sign Up</Link>
            </Message>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
  loggedIn: state.auth.isAuthenticated
});

export default withRouter<any>(
  connect(
    mapStateToProps,
    { loginRequest }
  )(
    reduxForm<{}, Props>({
      form: "login"
    })(Login)
  )
);
