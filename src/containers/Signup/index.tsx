import React from "react";
import {
  Button,
  Form,
  Grid,
  Header,
  Image,
  Message,
  Segment
} from "semantic-ui-react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { signUpRequest } from "../../actions/auth";
import { reduxForm, InjectedFormProps, Field } from "redux-form";
import TextField from "../../components/TextField";

interface StateProps {
  valueFromState: string;
}

interface Props extends StateProps {
  loading: boolean;
  loginRequest: any;
  signUpRequest: any;
  history: any;
  loggedIn: any;
}
interface ISignupValues {
  email: string;
  password: string;
  password_repeat: string;
}
class Signup extends React.Component<Props & InjectedFormProps<{}, Props>> {
  state = {
    token: localStorage.getItem("token")
  };

  componentDidMount() {
    if (this.props.loggedIn || this.state.token) {
      this.props.history.push("/");
    }
  }
  componentDidUpdate() {
    if (this.props.loggedIn || this.state.token) {
      this.props.history.push("/");
    }
  }

  handleSignUp = (values: ISignupValues | any): any => {
    this.props.signUpRequest(values);
  };
  render() {
    const { handleSubmit } = this.props;

    return (
      <div className="login-form">
        {/*
      Heads up! The styles below are necessary for the correct render of this example.
      You can do same with CSS, the main idea is that all the elements up to the `Grid`
      below must have a height of 100%.
    */}
        <style>{`
      body > div,
      body > div > div,
      body > div > div > div.login-form {
        height: 100%;
      }
    `}</style>
        <Grid
          textAlign="center"
          style={{ height: "100%" }}
          verticalAlign="middle"
        >
          <Grid.Column style={{ maxWidth: 450 }}>
            <Header as="h2" color="grey" textAlign="center">
              Sign Up
            </Header>
            <Form size="large" onSubmit={handleSubmit(this.handleSignUp)}>
              <Segment stacked>
                <Field
                  name="email"
                  component={TextField}
                  fluid
                  icon="user"
                  iconPosition="left"
                  placeholder="E-mail address"
                />
                <Field
                  name="password"
                  component={TextField}
                  fluid
                  icon="lock"
                  iconPosition="left"
                  placeholder="Password"
                  type="password"
                />
                <Field
                  name="password_repeat"
                  component={TextField}
                  fluid
                  icon="lock"
                  iconPosition="left"
                  placeholder="Repeat Password"
                  type="password"
                />

                <Button color="green" fluid size="large">
                  Sign up
                </Button>
              </Segment>
            </Form>
            <Message>
              If you have account <Link to="/login">Login</Link>
            </Message>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
  loggedIn: state.auth.isAuthenticated
});

export default connect(
  mapStateToProps,
  { signUpRequest }
)(reduxForm<{}, Props>({ form: "sign-up" })(Signup));
