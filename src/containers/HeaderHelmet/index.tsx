import React from "react";
import { Dropdown, Menu, Container, Image, Button } from "semantic-ui-react";
import { Link } from "react-router-dom";
import limeIcon from "../../assets/lime.png";
import { connect } from "react-redux";
import { logout } from "../../actions/auth";

interface IProps {
  getAllBooks: void;
}

class HeaderHelmet extends React.PureComponent<any, any> {
  handleLogout = () => {
    // console.log(this.props);
    // console.log("0");
    this.props.logout();
  };
  render() {
    return (
      <>
        <Menu fixed="top" color="blue" inverted style={{ height: 60 }}>
          <Container>
            <Menu.Item as="a" header>
              <Image
                size="mini"
                src={limeIcon}
                style={{ marginRight: "1.5em" }}
              />
              Project Name
            </Menu.Item>
            <Menu.Item as={Link} to="/">
              Home
            </Menu.Item>

            <Dropdown item simple text="Any Menu">
              <Dropdown.Menu>
                <Dropdown.Item as={Link} to="/contact-us">
                  Contact Us
                </Dropdown.Item>
                <Dropdown.Item as={Link} to="/about-us">
                  About Us
                </Dropdown.Item>
                <Dropdown.Divider />
                <Dropdown.Header>Header Item</Dropdown.Header>
                <Dropdown.Item>
                  <i className="dropdown icon" />
                  <span className="text">Submenu</span>
                  <Dropdown.Menu>
                    <Dropdown.Item>Contact Us</Dropdown.Item>
                    <Dropdown.Item>About Us</Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown.Item>
                <Dropdown.Item>List Item</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>

            <Menu.Item style={{ marginLeft: "auto" }}>
              <Button
                inverted
                basic
                icon="sign out"
                circular
                onClick={this.handleLogout}
              />
            </Menu.Item>
          </Container>
        </Menu>
      </>
    );
  }
}

const mapStateToProps = (state: any) => ({ ...state });
export default connect(
  mapStateToProps,
  { logout }
)(HeaderHelmet);
