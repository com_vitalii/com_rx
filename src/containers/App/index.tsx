import React from "react";
import HeaderHelmet from "../../containers/HeaderHelmet";
import HomePage from "../../components/HomePage";

interface IProps {
  getAllBooks: void;
}

class App extends React.PureComponent<any, any> {

  render() {
    return (
      <>
        <HeaderHelmet/>
        <HomePage />
      </>
    );
  }
}

export default App;
