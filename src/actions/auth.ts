import {
  LOGIN_START,
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  SIGN_UP_FAILED,
  SIGN_UP_START,
  SIGN_UP_SUCCESS,
  LOGOUT_START,
  LOGOUT_FAILED,
  LOGOUT_SUCCESS
} from "../constants/action-types";

export const loginRequest = (values: any) => ({
  type: LOGIN_START,
  payload: values
});
export const loginSuccess = (user: any) => ({
  type: LOGIN_SUCCESS,
  payload: user
});
export const loginFailure = () => ({
  type: LOGIN_FAILED
});

export const logout = () => ({
  type: LOGOUT_START
});
export const logoutSuccess = () => ({
  type: LOGOUT_SUCCESS
});
export const logoutFailed = () => ({
  type: LOGOUT_FAILED
});

export const signUpRequest = (values: any) => ({
  type: SIGN_UP_START,
  payload: values
});
export const signUpSuccess = () => ({
  type: SIGN_UP_SUCCESS
});
export const signUpFailure = () => ({
  type: SIGN_UP_FAILED
});
