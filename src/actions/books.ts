import {
  GET_ALL_BOOKS_START,
  GET_ALL_BOOKS_SUCCESS,
  GET_ALL_BOOKS_FAILURE
} from "../constants/action-types";

export const getAllBooks = () => ({
  type: GET_ALL_BOOKS_START
});

export const getAllBooksSuccess = (books: any) => ({
  type: GET_ALL_BOOKS_SUCCESS,
  payload: books
});

export const getAllBooksFailure = (msg: any) => ({
  type: GET_ALL_BOOKS_FAILURE,
  payload: msg
});
