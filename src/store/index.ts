import { createEpicMiddleware } from "redux-observable";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import rootReducer from "../reducers";
import rootEpic from "../epics";
const epicMiddleware = createEpicMiddleware();

const loggerMiddleware = () => (next: any) => (action: any) => {
  console.warn("Middleware trigered: ", action);
  next(action);
};

const middlewares = [epicMiddleware, loggerMiddleware];

const configureStore = () => {
  const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(...middlewares))
  );

  epicMiddleware.run(rootEpic);

  return store;
};

export default configureStore;
