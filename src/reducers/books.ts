import {
  GET_ALL_BOOKS_START,
  GET_ALL_BOOKS_SUCCESS,
  GET_ALL_BOOKS_FAILURE
} from "../constants/action-types";

const initialState: any = {
  books: [],
  loading: false
};

const books = (state = initialState, action: any) => {
  switch (action.type) {
    case GET_ALL_BOOKS_START:
      return {
        ...state,
        loading: true
      };
    case GET_ALL_BOOKS_SUCCESS:
      return {
        ...state,
        books: action.payload,
        loading: false
      };
    case GET_ALL_BOOKS_FAILURE:
      return {
        ...state,
        books: action.payload,
        loading: false
      };
    default:
      return state;
  }
};

export default books;
