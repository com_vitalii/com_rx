import {
  LOGIN_SUCCESS,
  LOGIN_START,
  LOGIN_FAILED,
  SIGN_UP_START,
  SIGN_UP_FAILED,
  SIGN_UP_SUCCESS,
  LOGOUT_START,
  LOGOUT_SUCCESS,
  LOGOUT_FAILED
} from "../constants/action-types";

const initialState: IAuthState = {
  isAuthenticated: false,
  token: null,
  logining: false
};

const auth = (state = initialState, action: any) => {
  const { type, payload } = action;
  switch (type) {
    case LOGIN_START:
      return {
        ...state,
        logining: true
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        logining: false
      };
    case LOGIN_FAILED:
      return {
        ...state,
        error: payload,
        logining: false
      };
    case SIGN_UP_START:
      return {
        ...state
      };
    case SIGN_UP_SUCCESS:
      return {
        ...state
      };
    case SIGN_UP_FAILED:
      return {
        ...state
      };
    case LOGOUT_START:
      return {
        ...state
      };
    case LOGOUT_SUCCESS:
      return {
        ...state,
        isAuthenticated: false,
        token: null
      };
    case LOGOUT_FAILED:
      return {
        ...state,
        error: "False"
      };
    default:
      return state;
  }
};

interface IAuthState {
  isAuthenticated: boolean;
  token: string | null;
  logining: boolean;
}

export default auth;
