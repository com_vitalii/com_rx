import { combineReducers } from "redux";
import { reducer as reduxFormReducer } from "redux-form";

import books from "./books";
import auth from "./auth";

const rootReducer = combineReducers({ form: reduxFormReducer, books, auth });

export default rootReducer;
