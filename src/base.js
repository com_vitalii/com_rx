import firebase from "firebase";

const config = firebase.initializeApp({
  apiKey: "AIzaSyDGV_s6Mf48jJ2TPPQFBD4rNh4D5F-yUOg",
  authDomain: "rex-test-28bf0.firebaseapp.com",
  databaseURL: "https://rex-test-28bf0.firebaseio.com",
  projectId: "rex-test-28bf0",
  storageBucket: "rex-test-28bf0.appspot.com",
  messagingSenderId: "566294626221"
});
if (!firebase.apps.length) {
  firebase.initializeApp(config);
}
export const provider = new firebase.auth.GoogleAuthProvider();
export const auth = firebase.auth();

export default firebase;
