import { Observable, of } from "rxjs";
// import "rxjs/add/operator/mergeMap";
import { ofType } from "redux-observable";
// import "rxjs/add/operator/map";
import { map, mergeMap, switchMap, catchError } from "rxjs/operators";
import { ajax } from "rxjs/ajax";
import { GET_ALL_BOOKS_START } from "../constants/action-types";
import { getAllBooksSuccess, getAllBooksFailure } from "../actions/books";

const url = "https://evening-citadel-85778.herokuapp.com/whiskey/";

interface IAction {
  type: string;
  payload?: any;
}

export const booksEpic = (action$: Observable<any>) => {
  return action$.pipe(
    ofType(GET_ALL_BOOKS_START),
    switchMap(
      (): Observable<IAction> =>
        ajax.getJSON(url).pipe(
          map(
            (response: any): IAction =>
              response.results.map((item: any) => ({
                id: item.id
              })),
            catchError((error: any) => {
              return of(getAllBooksFailure(error));
            })
          ),
          map(
            (books: any): IAction => {
              return getAllBooksSuccess(books);
            }
          )
        )
    )
  );
};
