import { Observable, from, of, timer } from "rxjs";
import { LOGIN_START, SIGN_UP_START, LOGOUT_START } from "../constants/action-types";
import history from "../history";
import {
  loginSuccess,
  signUpSuccess,
  signUpFailure,
  loginFailure,
  logoutSuccess,
  logoutFailed
} from "../actions/auth";
import {
  map,
  catchError,
  tap,
  mergeMap,
  take,
  delay,
  takeUntil,
  first
} from "rxjs/operators";
import { ofType } from "redux-observable";
import { auth } from "../base.js";

interface IAction {
  type: string;
  payload?: any;
}

export const loginEpic = (action$: Observable<any>) => {
  return action$.pipe(
    ofType(LOGIN_START),
    mergeMap(action => {
      const { email, password } = action.payload;
      return from(auth.signInWithEmailAndPassword(email, password)).pipe(
        map(
          (response: any): IAction => {
            const user = response.user;
            user.getIdToken().then((token: any) => {
              localStorage.setItem("token", JSON.stringify(token));
            });
            return loginSuccess(user);
          }
        ),
        tap(() => history.push("/ssss")),
        catchError(error => of(loginFailure()))
      );
    })
  );
};

const resetAuth = () => {
  console.log("1");
  localStorage.removeItem("token");
};

export const logoutEpic = (action$: Observable<any>) => {
  return action$.pipe(
    ofType(LOGOUT_START),
    tap(
      (): any => {
        resetAuth();
      }
    ),
    tap(
      (): any => {
        history.push("/login");
      }
    ),
    map(() => logoutSuccess()),
    catchError(() => of(logoutFailed()))
  );
};

export const signUpEpic = (action$: Observable<any>) => {
  return action$.pipe(
    ofType(SIGN_UP_START),
    mergeMap(action => {
      const { email, password } = action.payload;
      return from(auth.createUserWithEmailAndPassword(email, password)).pipe(
        map(
          (response: any): IAction => {
            return signUpSuccess();
          }
        ),
        tap(() => {
          history.push("/login");
        }),
        catchError((error: any) => of(signUpFailure()))
      );
    })
  );
};
