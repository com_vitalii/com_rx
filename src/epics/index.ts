import { combineEpics } from "redux-observable";
import { booksEpic } from "./books";
import { loginEpic, signUpEpic, logoutEpic } from "./auth";

const rootEpic = combineEpics(loginEpic, signUpEpic, booksEpic, logoutEpic);

export default rootEpic;
