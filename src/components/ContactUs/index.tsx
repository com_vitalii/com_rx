import React from "react";
import { ContactSC } from "./styles";
import { Button } from "semantic-ui-react";

const ContactUs = () => {
  return (
    <ContactSC>
      <div>Contact us</div>
      <Button>Contact Us button</Button>
    </ContactSC>
  );
};

export default ContactUs;
