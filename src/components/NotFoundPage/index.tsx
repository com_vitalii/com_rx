import React from "react";
import {
  NotFoundPageSC,
  NotFoundPageHeaderSC,
  NotFoundPageDescrSC
} from "./styles";
import { Link } from "react-router-dom";

const NotFoundPage = () => (
  <NotFoundPageSC>
    <NotFoundPageHeaderSC>404</NotFoundPageHeaderSC>
    <NotFoundPageDescrSC>Not Found Page</NotFoundPageDescrSC>
    <Link to="/">Go home</Link>
  </NotFoundPageSC>
);

export default NotFoundPage;
