import styled from "styled-components";

export const NotFoundPageSC = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100vh;
  padding: 60px 100px;
`;

export const NotFoundPageHeaderSC = styled.p`
  font-size: 40px;
`;
export const NotFoundPageDescrSC = styled.p`
  font-size: 18px;
  margin-bottom: 0;
`;
