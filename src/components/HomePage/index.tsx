import React from "react";
import manIcon from "../../assets/man.png";
import {
  Grid,
  Container,
  Image,
  Header,
  Segment,
  Divider,
  List
} from "semantic-ui-react";
import {
  HomeMainSC,
  HomeMainHeaderSC,
  HomeMainDescriptionSC
} from "./styles";

class HomePage extends React.Component {
  render() {
    return (
      <>
        <HomeMainSC>
          <HomeMainHeaderSC>Site test</HomeMainHeaderSC>
          <HomeMainDescriptionSC>
            Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist in
            der Industrie bereits der Standard Demo-Text seit 1500, als ein
            unbekannter Schriftsteller eine Hand voll Wörter nahm und diese
            durcheinander warf um ein Musterbuch zu erstellen. Es hat nicht nur
            5 Jahrhunderte überlebt, sondern auch in Spruch
          </HomeMainDescriptionSC>
        </HomeMainSC>
        <Container text style={{ marginTop: "7em" }}>
          <Header as="h1">Standard Demo-Text seit 1500</Header>
          <p>
            Standard Demo-Text seit 1500, als ein unbekannter Schriftsteller
            eine Hand voll Wörter nahm und diese durcheinander warf um ein
            Musterbuch zu erstellen. Es hat nicht nur 5 Jahrhunderte überlebt,
            sondern auch in Spruch in die elektronische Schriftbearbeitung
            geschafft (bemerke, nahezu unverändert). Bekannt wurde es 1960, mit
            dem erscheinen von "Letraset", welches Passagen von Lorem Ipsum
            enhielt, so wie Desktop Software wie "Aldus PageMaker" - ebenfalls
            mit Lorem Ipsum.
          </p>
          <p>
            Glauben oder nicht glauben, Lorem Ipsum ist nicht nur ein zufälliger
            Text. Er hat Wurzeln aus der Lateinischen Literatur von 45 v. Chr,
            was ihn über 2000 Jahre alt macht. Richar McClintock, ein
            Lateinprofessor des Hampden-Sydney College in Virgnia untersuche
            einige undeutliche Worte, "consectetur", einer Lorem Ipsum Passage
            und fand eine unwiederlegbare Quelle. Lorem Ipsum komm aus der
            Sektion 1.10.32 und 1.10.33 des "de Finibus Bonorum et Malorum" (Die
            Extreme von Gut und Böse) von Cicero, geschrieben 45 v. Chr. Dieses
            Buch ist Abhandlung der Ethiktheorien, sehr bekannt wärend der
            Renaissance. Die erste Zeile des Lorem Ipsum, "Lorem ipsum dolor sit
            amet...", kommt aus einer Zeile der Sektion 1.10.32. Der
            Standardteil von Lorem Ipsum, genutzt seit 1500, ist reproduziert
            für die, die es interessiert. Sektion 1.10.32 und 1.10.33 von "de
            Finibus Bonorum et Malroum" von Cicero sind auch reproduziert in
            ihrer Originalform, abgeleitet von der Englischen Version aus von
            1914 (H. Rackham) Glauben oder nicht glauben, Lorem Ipsum ist nicht
            nur ein zufälliger Text. Er hat Wurzeln aus der Lateinischen
            Literatur von 45 v. Chr, was ihn über 2000 Jahre alt macht. Richar
            McClintock, ein Lateinprofessor des Hampden-Sydney College in
            Virgnia untersuche einige undeutliche Worte, "consectetur", einer
            Lorem Ipsum Passage und fand eine unwiederlegbare Quelle. Lorem
            Ipsum komm aus der Sektion 1.10.32 und 1.10.33 des "de Finibus
            Bonorum et Malorum" (Die Extreme von Gut und Böse) von Cicero,
            geschrieben 45 v. Chr. Dieses Buch ist Abhandlung der Ethiktheorien,
            sehr bekannt wärend der Renaissance. Die erste Zeile des Lorem
            Ipsum, "Lorem ipsum dolor sit amet...", kommt aus einer Zeile der
            Sektion 1.10.32. Der Standardteil von Lorem Ipsum, genutzt seit
            1500, ist reproduziert für die, die es interessiert. Sektion 1.10.32
            und 1.10.33 von "de Finibus Bonorum et Malroum" von Cicero sind auch
            reproduziert in ihrer Originalform, abgeleitet von der Englischen
            Version aus von 1914 (H. Rackham) Glauben oder nicht glauben, Lorem
            Ipsum ist nicht nur ein zufälliger Text. Er hat Wurzeln aus der
            Lateinischen Literatur von 45 v. Chr, was ihn über 2000 Jahre alt
            macht. Richar McClintock, ein Lateinprofessor des Hampden-Sydney
            College in Virgnia untersuche einige undeutliche Worte,
            "consectetur", einer Lorem Ipsum Passage und fand eine
            unwiederlegbare Quelle. Lorem Ipsum komm aus der Sektion 1.10.32 und
            1.10.33 des "de Finibus Bonorum et Malorum" (Die Extreme von Gut und
            Böse) von Cicero, geschrieben 45 v. Chr. Dieses Buch ist Abhandlung
            der Ethiktheorien, sehr bekannt wärend der Renaissance. Die erste
            Zeile des Lorem Ipsum, "Lorem ipsum dolor sit amet...", kommt aus
            einer Zeile der Sektion 1.10.32. Der Standardteil von Lorem Ipsum,
            genutzt seit 1500, ist reproduziert für die, die es interessiert.
            Sektion 1.10.32 und 1.10.33 von "de Finibus Bonorum et Malroum" von
            Cicero sind auch reproduziert in ihrer Originalform, abgeleitet von
            der Englischen Version aus von 1914 (H. Rackham) Glauben oder nicht
            glauben, Lorem Ipsum ist nicht nur ein zufälliger Text. Er hat
            Wurzeln aus der Lateinischen Literatur von 45 v. Chr, was ihn über
            2000 Jahre alt macht. Richar McClintock, ein Lateinprofessor des
            Hampden-Sydney College in Virgnia untersuche einige undeutliche
            Worte, "consectetur", einer Lorem Ipsum Passage und fand eine
            unwiederlegbare Quelle. Lorem Ipsum komm aus der Sektion 1.10.32 und
            1.10.33 des "de Finibus Bonorum et Malorum" (Die Extreme von Gut und
            Böse) von Cicero, geschrieben 45 v. Chr. Dieses Buch ist Abhandlung
            der Ethiktheorien, sehr bekannt wärend der Renaissance. Die erste
            Zeile des Lorem Ipsum, "Lorem ipsum dolor sit amet...", kommt aus
            einer Zeile der Sektion 1.10.32. Der Standardteil von Lorem Ipsum,
            genutzt seit 1500, ist reproduziert für die, die es interessiert.
            Sektion 1.10.32 und 1.10.33 von "de Finibus Bonorum et Malroum" von
            Cicero sind auch reproduziert in ihrer Originalform, abgeleitet von
            der Englischen Version aus von 1914 (H. Rackham)
          </p>
        </Container>

        <Segment style={{ padding: "0em", paddingTop: "100px" }} vertical>
          <Grid celled="internally" columns="equal" stackable>
            <Grid.Row textAlign="center">
              <Grid.Column style={{ paddingBottom: "5em", paddingTop: "5em" }}>
                <Header as="h3" style={{ fontSize: "2em" }}>
                  "Dieses Buch is"
                </Header>
                <p style={{ fontSize: "1.33em" }}>Dieses Buch ist Abhandlung</p>
              </Grid.Column>
              <Grid.Column style={{ paddingBottom: "5em", paddingTop: "5em" }}>
                <Header as="h3" style={{ fontSize: "2em" }}>
                  "Der Standardteil von Lorem Ipsum Dieses Buch ist Abhandlung"
                </Header>
                <p style={{ fontSize: "1.33em" }}>
                  <Image avatar src={manIcon} />
                  <b>Vitalik </b>
                  Dieses Buch ist Abhandlung
                </p>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>

        <Segment
          inverted
          vertical
          style={{ margin: "0", padding: "5em 0em 1.5em" }}
        >
          <Container textAlign="center">
            <Grid divided inverted stackable>
              <Grid.Row>
                <Grid.Column width={3}>
                  <Header inverted as="h4" content="Group 1" />
                  <List link inverted>
                    <List.Item as="a">Link One</List.Item>
                    <List.Item as="a">Link Two</List.Item>
                    <List.Item as="a">Link Three</List.Item>
                    <List.Item as="a">Link Four</List.Item>
                  </List>
                </Grid.Column>
                <Grid.Column width={3}>
                  <Header inverted as="h4" content="Group 2" />
                  <List link inverted>
                    <List.Item as="a">Link One</List.Item>
                    <List.Item as="a">Link Two</List.Item>
                    <List.Item as="a">Link Three</List.Item>
                    <List.Item as="a">Link Four</List.Item>
                  </List>
                </Grid.Column>
                <Grid.Column width={3}>
                  <Header inverted as="h4" content="Group 3" />
                  <List link inverted>
                    <List.Item as="a">Link One</List.Item>
                    <List.Item as="a">Link Two</List.Item>
                    <List.Item as="a">Link Three</List.Item>
                    <List.Item as="a">Link Four</List.Item>
                  </List>
                </Grid.Column>
                <Grid.Column width={7}>
                  <Header inverted as="h4" content="Footer Header" />
                  <p>
                    Extra space for a call to action inside the footer that
                    could help re-engage users.
                  </p>
                </Grid.Column>
              </Grid.Row>
            </Grid>

            <Divider inverted section />
            <List horizontal inverted divided link>
              <List.Item as="a" href="#">
                Site Map
              </List.Item>
              <List.Item as="a" href="#">
                Contact Us
              </List.Item>
              <List.Item as="a" href="#">
                Terms and Conditions
              </List.Item>
              <List.Item as="a" href="#">
                Privacy Policy
              </List.Item>
            </List>
          </Container>
        </Segment>
      </>
    );
  }
}

export default HomePage;
