import styled from "styled-components";

export const HomeMainSC = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100vh;
  padding: 60px 100px;
  background: #26272b;
`;

export const HomeMainHeaderSC = styled.h1`
  color: #fff;
`;

export const HomeMainDescriptionSC = styled.div`
  color: #fff;
  font-size: 14px;
  text-align: center;
  max-width: 800px;
`;
