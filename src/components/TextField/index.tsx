import React from "react";
import {
  Button,
  Form,
  Grid,
  Header,
  Image,
  Message,
  Segment
} from "semantic-ui-react";

const TextField = ({
  input,
  icon,
  iconPosition,
  placeholder,
  type,
  ...rest
}: any) => (
  <Form.Input
    {...rest}
    {...input}
    icon={icon}
    iconPosition={iconPosition}
    placeholder={placeholder}
    type={type}
  />
);

export default TextField;
